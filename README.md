# smart-wallet-server
This is repository for semestral project at FIM UHK, subject KPPRO

#Steps
1. clone repository from GitLab
2. Install Gradle form https://gradle.org/
3. Reopen Intellij IDEA to load project with Gradle
4. Click on start icon (green triangle) to start Application

App uses H2 embedded database for fast development

In you build log will be this line `Using generated security password: "some password"`

You will need this for accessing HAL REST explorer which is accessible via http://localhost:8080/browser/index.html
Credentials: username: user, password: <password form build log>

For accessing database use http://localhost:8080/h2-console/

!! Use this `jdbc:h2:mem:testdb` as JDBC URL

Credentials to DB are stored in  `/scr/main/resources/application.properties`

![Image of config for DB](https://i.imgur.com/vtGeRKx.png)

Commands

`/users` -- this will add one record to DB and then list all users in DB