package com.smartwallet.model;

import javax.persistence.*;

@Entity
public class Invitation {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Budget budget;

    @ManyToOne
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
