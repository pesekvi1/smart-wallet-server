package com.smartwallet.model;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Budget {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    @NotNull(message = "Please provide name of the budget.")
    @Length(min = 5, message = "Name must be at least 5 characters long.")
    private String name;

    @ManyToOne
    private User owner;

    @OneToMany(orphanRemoval = true, mappedBy = "budget", cascade = CascadeType.REMOVE)
    private List<BudgetItem> budgetItems;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
