package com.smartwallet.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class BudgetItem {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Budget budget;

    @Column
    private String name;

    @Column
    private Double price;

    @Column
    private Date date;

    @OneToOne
    private Category category;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Budget getBudget() {
        return budget;
    }

    public void setBudget(Budget budget) {
        this.budget = budget;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
