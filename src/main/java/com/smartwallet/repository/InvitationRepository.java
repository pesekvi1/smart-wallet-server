package com.smartwallet.repository;

import com.smartwallet.model.Budget;
import com.smartwallet.model.Invitation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface InvitationRepository extends JpaRepository<Invitation, Long> {

    @Query("select i from Invitation i where i.budget.id = ?1")
    List<Invitation> findInvitationsByBudgetId(Long budgetId);
}
