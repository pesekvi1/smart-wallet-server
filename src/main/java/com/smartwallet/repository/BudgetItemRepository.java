package com.smartwallet.repository;

import com.smartwallet.model.Budget;
import com.smartwallet.model.BudgetItem;
import com.smartwallet.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BudgetItemRepository extends JpaRepository<BudgetItem, Long> {

    @Query("select b from BudgetItem b  where b.category = ?1")
    List<BudgetItem> findByCategory(Category category);

    @Query("select b from BudgetItem b where b.budget = ?1")
    List<BudgetItem> findBudgetItemsByBudget(Budget budget);
}
