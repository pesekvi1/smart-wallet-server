package com.smartwallet.domain;

public enum UserType {
    Google("GOOGLE"),
    Facebook("FACEBOOK");

    private final String type;

    private UserType(String type) {
        this.type = type;
    }

    public String getValue() {
        return type;
    }

    @Override
    public String toString() {
        return "UserType{" +
                "type='" + type + '\'' +
                '}';
    }
}
