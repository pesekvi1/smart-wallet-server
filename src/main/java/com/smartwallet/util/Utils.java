package com.smartwallet.util;

import com.smartwallet.model.User;
import org.springframework.security.core.context.SecurityContextHolder;

public class Utils {

    public static String getAuthenticatedUserName() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    public static boolean isAuthenticatedUserOwnerOfEntity(User user) {
        String authUser = SecurityContextHolder.getContext().getAuthentication().getName();
        return user.getEmail().equals(authUser);
    }
}
