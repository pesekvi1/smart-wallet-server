package com.smartwallet.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class EntiyNotOwnedByCurrentUser extends Exception {

    public EntiyNotOwnedByCurrentUser(String message) {
        super(message);
    }
}
