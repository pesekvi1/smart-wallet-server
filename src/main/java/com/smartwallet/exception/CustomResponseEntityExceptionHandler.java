package com.smartwallet.exception;

import com.smartwallet.exception.error.ErrorDetails;
import javassist.NotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails();
        errorDetails.setHttpStatus(HttpStatus.BAD_REQUEST);
        errorDetails.setMessage("Validation Error");
        errorDetails.addValidationError(ex.getBindingResult().getFieldErrors());
        errorDetails.addObjectErrors(ex.getBindingResult().getGlobalErrors());
        return returnResponseEntity(errorDetails);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
       ErrorDetails errorDetails = new ErrorDetails();
       errorDetails.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
       errorDetails.setMessage(ex.getMessage());
       return returnResponseEntity(errorDetails);
    }

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<Object> handleBudgetNotFound(Exception ex, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails();
        errorDetails.setHttpStatus(HttpStatus.NOT_FOUND);
        errorDetails.setMessage(ex.getMessage());
        return returnResponseEntity(errorDetails);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails();
        errorDetails.setHttpStatus(HttpStatus.BAD_REQUEST);
        errorDetails.setMessage(ex.getMessage());
        return returnResponseEntity(errorDetails);
    }

    @ExceptionHandler(EntiyNotOwnedByCurrentUser.class)
    protected ResponseEntity<Object> handleUserDoesNotOwnEntity(Exception ex, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails();
        errorDetails.setHttpStatus(HttpStatus.FORBIDDEN);
        errorDetails.setMessage(ex.getMessage());
        return returnResponseEntity(errorDetails);
    }

    @ExceptionHandler(ItemsRelatedToCategoryExist.class)
    protected ResponseEntity<Object> handleItemsRelatedToCategory(Exception ex, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails();
        errorDetails.setHttpStatus(HttpStatus.BAD_REQUEST);
        errorDetails.setMessage(ex.getLocalizedMessage());
        return returnResponseEntity(errorDetails);
    }

    public ResponseEntity<Object> returnResponseEntity(ErrorDetails errorDetails) {
        return new ResponseEntity<Object>(errorDetails, new HttpHeaders(), errorDetails.getHttpStatus());
    }
}
