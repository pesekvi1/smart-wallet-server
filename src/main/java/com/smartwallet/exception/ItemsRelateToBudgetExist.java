package com.smartwallet.exception;

public class ItemsRelateToBudgetExist extends RuntimeException {

    public ItemsRelateToBudgetExist(String message) {
        super(message);
    }
}
