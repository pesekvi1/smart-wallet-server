package com.smartwallet.exception.error;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ErrorDetails {

    private HttpStatus httpStatus;
    private LocalDateTime timestamp;
    private String message;
    private String details;
    private List<Error> errors;

    public ErrorDetails() {
        this.timestamp = LocalDateTime.now();
    }

    public ErrorDetails(HttpStatus httpStatus, Throwable ex) {
        super();
        this.httpStatus = httpStatus;
        this.message = "Unexpected error";
        this.details = ex.getLocalizedMessage();
    }

    public ErrorDetails (HttpStatus httpStatus, String message, Throwable ex) {
        super();
        this.httpStatus = httpStatus;
        this.message = message;
        this.details = ex.getLocalizedMessage();
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }

    public void addError(Error error) {
        if (this.errors == null) {
            this.errors = new ArrayList<>();
        }
        this.errors.add(error);
    }

    public void addValidationError(List<FieldError> errors) {
        errors.forEach(this::addValidationError);
    }

    public void addValidationError(FieldError error) {
        addError(new ValidationError(
                error.getObjectName(),
                error.getField(),
                error.getRejectedValue(),
                error.getDefaultMessage()
        ));
    }

    public void addObjectError(ObjectError error) {
        addError(new ValidationError(
                error.getObjectName(),
                error.getDefaultMessage()
        ));
    }

    public void addObjectErrors(List<ObjectError> errors) {
        errors.forEach(this::addObjectError);
    }
}
