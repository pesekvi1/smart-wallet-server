package com.smartwallet.exception;

public class ItemsRelatedToCategoryExist extends RuntimeException {

    public ItemsRelatedToCategoryExist(String message) {
        super(message);
    }
}
