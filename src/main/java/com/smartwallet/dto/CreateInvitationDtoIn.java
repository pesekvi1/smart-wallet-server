package com.smartwallet.dto;

import javax.validation.constraints.NotNull;

public class CreateInvitationDtoIn {

    @NotNull
    private Long budgetId;

    @NotNull
    private String userEmail;

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
