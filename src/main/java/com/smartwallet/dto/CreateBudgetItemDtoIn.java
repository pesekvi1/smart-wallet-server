package com.smartwallet.dto;

import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.validation.constraints.NotNull;

public class CreateBudgetItemDtoIn {

    @NotNull
    private Long budgetId;

    @NotNull
    private Long categoryId;

    @NotNull
    private String itemName;

    @NotNull
    private Double price;

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
