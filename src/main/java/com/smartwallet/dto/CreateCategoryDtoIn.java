package com.smartwallet.dto;

public class CreateCategoryDtoIn {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
