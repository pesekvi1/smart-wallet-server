package com.smartwallet.controller;

import com.smartwallet.dto.CreateCategoryDtoIn;
import com.smartwallet.exception.EntityNotFoundException;
import com.smartwallet.exception.ItemsRelatedToCategoryExist;
import com.smartwallet.model.BudgetItem;
import com.smartwallet.model.Category;
import com.smartwallet.repository.BudgetItemRepository;
import com.smartwallet.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "category")
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private BudgetItemRepository budgetItemRepository;

    @PostMapping
    public ResponseEntity<Category> create(@RequestBody @Valid CreateCategoryDtoIn dtoIn) {
        Category category = new Category();
        category.setName(dtoIn.getName());

        try {
            categoryRepository.save(category);
        } catch (Exception e) {
            //TODO doplnit logovani
            throw e;
        }

        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Category>> get() {
        List<Category> categories = categoryRepository.findAll();
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<Category> delete(@RequestParam Long categoryId) throws EntityNotFoundException {
        Optional<Category> categoryOptional = categoryRepository.findById(categoryId);

        if (!categoryOptional.isPresent()) {
            throw new EntityNotFoundException(String.format("Category with id: %d was not found.", categoryId));
        }

        List<BudgetItem> items = budgetItemRepository.findByCategory(categoryOptional.get());

        if (items.size() > 0) {
            throw new ItemsRelatedToCategoryExist(String.format("Cannot delete category with id: %d. There are %d items with this category assigned", categoryId, items.size()));
        }

        try {
            categoryRepository.delete(categoryOptional.get());
        } catch (Exception e) {
            //TODO logovat
            throw e;
        }

        return new ResponseEntity<>(categoryOptional.get(), HttpStatus.OK);
    }
}
