package com.smartwallet.controller;

import com.smartwallet.exception.EntityNotFoundException;
import com.smartwallet.model.Budget;
import com.smartwallet.model.User;
import com.smartwallet.repository.BudgetRepository;
import com.smartwallet.repository.UserRepository;
import com.smartwallet.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@Validated
public class BudgetController {

    private static final Logger logger = LoggerFactory.getLogger(BudgetController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BudgetRepository budgetRepository;


    @PostMapping(path = "/budget")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Budget> createBudget(@Valid @RequestBody Budget budget) {
        logger.info("Adding budget {}", budget.toString());
        String email = Utils.getAuthenticatedUserName();
        User owner = null;
        try {
            owner = userRepository.findByEmail(email);
        } catch (Exception e) {
            logger.error("Exception occured during useCase POST /budget. Cannot find currentUser in DB", e);
        }
        budget.setOwner(owner);

        try {
            budgetRepository.save(budget);
        } catch (Exception e) {
            logger.debug("Error during saving budget to database", e);
        }
        return new ResponseEntity<>(budget, HttpStatus.OK);
    }

    @GetMapping(path = "/budget")
    public ResponseEntity<Budget> getBudget(@RequestParam(value = "id",required = false) Optional<Long> id) throws EntityNotFoundException {
        if (id.isPresent()) {
            Optional<Budget> budgetOptional = budgetRepository.findById(id.get());

            if (!budgetOptional.isPresent()) {
                throw new EntityNotFoundException(String.format("Budget with id: %d was not found.", id));
            }
            return new ResponseEntity(budgetOptional.get(), HttpStatus.OK);
        }

        return new ResponseEntity(budgetRepository.findAll(),HttpStatus.OK);
    }

    @DeleteMapping(path = "/budget")
    public ResponseEntity<Budget> deleteBudget(@RequestParam(value ="id")Long id) throws EntityNotFoundException {
        Optional<Budget> budgetOptional = budgetRepository.findById(id);
        if (!budgetOptional.isPresent()) {
            throw new EntityNotFoundException(String.format("Budget with id: %d was not found.", id));
        }
        budgetRepository.delete(budgetOptional.get());
        return new ResponseEntity(budgetOptional.get(),HttpStatus.OK);

    }


}
