package com.smartwallet.controller;

import com.smartwallet.dto.CreateInvitationDtoIn;
import com.smartwallet.exception.EntityNotFoundException;
import com.smartwallet.model.Budget;
import com.smartwallet.model.Invitation;
import com.smartwallet.model.User;
import com.smartwallet.repository.BudgetRepository;
import com.smartwallet.repository.InvitationRepository;
import com.smartwallet.repository.UserRepository;
import com.smartwallet.util.Utils;
import com.smartwallet.util.email.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class InvitationController {

    private static final Logger logger = LoggerFactory.getLogger(InvitationController.class);

    @Autowired
    private InvitationRepository invitationRepository;

    @Autowired
    private BudgetRepository budgetRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmailService emailService;

    @PostMapping(path = "/invitation")
    public ResponseEntity<Invitation> create(@RequestBody @Valid CreateInvitationDtoIn dtoIn) throws EntityNotFoundException {
        Optional<Budget> budget = budgetRepository.findById(dtoIn.getBudgetId());
        Optional<User> user = Optional.ofNullable(userRepository.findByEmail(dtoIn.getUserEmail()));

        String currentUser = Utils.getAuthenticatedUserName();

        if (!budget.isPresent()) {
            logger.error("Budget with id: {} was not found", dtoIn.getBudgetId());
            throw new EntityNotFoundException("Budget with id: " + dtoIn.getBudgetId() + " was not found");
        }

        String ownerOfBudget = budget.get().getOwner().getEmail();

        if (!ownerOfBudget.equals(currentUser)) {
            logger.info("User {} tried to create invitation of budget that he does not own. Budget: {}", currentUser, budget.get());

        }

        if (!user.isPresent()) {
            logger.error("User with email: was not found", dtoIn.getUserEmail());
            throw new EntityNotFoundException("User with email: " + dtoIn.getUserEmail() + " was not found");
        }

        Invitation invitation = new Invitation();
        invitation.setBudget(budget.get());
        invitation.setUser(user.get());

        try {
            invitationRepository.save(invitation);
        } catch (Exception e) {
            logger.error("Error occured when saving of invitation: {}", invitation);
        }

        emailService.sendMail(user.get().getEmail(), "Invitation to Budget", String.format("User %s wants to share a budget with you. Sign-in here -> http://localhost:3000/budget/%d", currentUser, budget.get().getId()));

        return new ResponseEntity<>(invitation, HttpStatus.OK);
    }

    @DeleteMapping(path = "/invitation")
    public ResponseEntity<Invitation> delete(@RequestParam Long invitationId) throws EntityNotFoundException {
        Optional<Invitation> optionalInvitation = invitationRepository.findById(invitationId);
        if (!optionalInvitation.isPresent()) {
            throw new EntityNotFoundException("Invitation with Id: " + invitationId + " was not found");
        }

        User owner = optionalInvitation.get().getBudget().getOwner();

        if (!Utils.isAuthenticatedUserOwnerOfEntity(owner)) {
            logger.warn("User {} tried to delete invitation that he does not own. Invitation: {}", Utils.getAuthenticatedUserName(), optionalInvitation.get());
        }

        try {
            invitationRepository.delete(optionalInvitation.get());
        } catch (Exception e) {
            logger.error("Error when deleting invitation {}", e.getLocalizedMessage());
            throw e;
        }

        return new ResponseEntity<>(optionalInvitation.get(), HttpStatus.OK);
    }
}
