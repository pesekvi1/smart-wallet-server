package com.smartwallet.controller;

import com.smartwallet.dto.CreateBudgetItemDtoIn;
import com.smartwallet.exception.EntityNotFoundException;
import com.smartwallet.model.Budget;
import com.smartwallet.model.BudgetItem;
import com.smartwallet.model.Category;
import com.smartwallet.repository.BudgetItemRepository;
import com.smartwallet.repository.BudgetRepository;
import com.smartwallet.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
public class BudgetItemController {

    @Autowired
    private BudgetRepository budgetRepository;

    @Autowired
    private BudgetItemRepository budgetItemRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @PostMapping(path = "/item")
    public ResponseEntity<BudgetItem> create(@RequestBody @Valid CreateBudgetItemDtoIn dtoIn) throws EntityNotFoundException {
        Optional<Budget> budgetOptional = budgetRepository.findById(dtoIn.getBudgetId());

        if (!budgetOptional.isPresent()) {
            throw new EntityNotFoundException(String.format("Budget with id: %d was not found.", dtoIn.getBudgetId()));
        }

        Optional<Category> categoryOptional = categoryRepository.findById(dtoIn.getCategoryId());

        if (!categoryOptional.isPresent()) {
            throw new EntityNotFoundException(String.format("Category with id: %d was not found.", dtoIn.getCategoryId()));
        }

        BudgetItem budgetItem = new BudgetItem();
        budgetItem.setBudget(budgetOptional.get());
        budgetItem.setCategory(categoryOptional.get());
        budgetItem.setName(dtoIn.getItemName());
        budgetItem.setPrice(dtoIn.getPrice());
        budgetItem.setDate(new Date());

        try {
            budgetItemRepository.save(budgetItem);
        } catch (Exception e) {
            //TODO logovat
            throw e;
        }

        return new ResponseEntity<>(budgetItem, HttpStatus.OK);
    }

    @GetMapping(path = "item")
    public ResponseEntity<List<BudgetItem>> get(@RequestParam Long id) throws EntityNotFoundException {
        Optional<Budget> budgetOptional = budgetRepository.findById(id);

        if (!budgetOptional.isPresent()) {
            throw new EntityNotFoundException(String.format("Budget with id %d was not found.", id));
        }

        List<BudgetItem> budgetItems = budgetItemRepository.findBudgetItemsByBudget(budgetOptional.get());

        return new ResponseEntity<>(budgetItems, HttpStatus.OK);
    }

    @DeleteMapping(path = "/item")
    public ResponseEntity<BudgetItem> delete(@RequestParam Long id) throws EntityNotFoundException {
        Optional<BudgetItem> optionalBudgetItem = budgetItemRepository.findById(id);

        if (!optionalBudgetItem.isPresent()) {
            throw new EntityNotFoundException(String.format("Budget item with id %d was not found.", id));
        }

        try {
            budgetItemRepository.delete(optionalBudgetItem.get());
        } catch (Exception e) {
            throw e;
        }

        return new ResponseEntity<>(optionalBudgetItem.get(), HttpStatus.OK);
    }
}
